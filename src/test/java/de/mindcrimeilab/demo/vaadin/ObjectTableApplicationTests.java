package de.mindcrimeilab.demo.vaadin;

import com.vaadin.data.Container;
import com.vaadin.data.Property;
import com.vaadin.data.util.BeanItem;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.AbstractComponent;
import com.vaadin.ui.Component;
import com.vaadin.ui.Grid;
import org.hamcrest.Matchers;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class ObjectTableApplicationTests {
    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock
    private ViewChangeEvent viewChangeEvent;

    private ObjectTableView otv = null;
    private Grid detail = null;
    private Grid list = null;

    @Before
    public void setup() {
        otv = new ObjectTableView();
        otv.init();
        otv.enter(viewChangeEvent);

        Component tmpDetail = null;
        Component tmpList = null;
        for (Component c : otv) {
            switch (c.getId()) {
                case ObjectTableView.OBJECT_DETAIL_ID:
                    tmpDetail = c;
                    break;
                case ObjectTableView.OBJECT_LIST_ID:
                    tmpList = c;
                    break;
                default:
                    // nothing
            }
        }

        assertThat(tmpDetail, notNullValue());
        assertThat(tmpList, notNullValue());

        assertThat(tmpDetail, Matchers.instanceOf(Grid.class));
        assertThat(tmpList, Matchers.instanceOf(Grid.class));

        list = (Grid) tmpList;
        detail = (Grid) tmpDetail;
    }


    @After
    public void tearDown() {
        list = null;
        detail = null;
        otv = null;
    }

    @Test
    public void contextLoads() {

        Container.Indexed source = list.getContainerDataSource();
        assertThat(2, is(source.size()));

        boolean expectedContentInList = source.getItemIds().stream()
                .map(source::getItem)
                .filter(c -> c instanceof BeanItem)
                .map(bi -> ((BeanItem) bi).getBean())
                .allMatch(b -> detail.equals(b) || list.equals(b));

        assertTrue(expectedContentInList);

        otv.onItemClicked(new ItemClickEvent(list, source.getItem(list), list, list, null));

        validate(detail.getContainerDataSource(), list);

        otv.onItemClicked(new ItemClickEvent(list, source.getItem(detail), detail, detail, null));

        validate(detail.getContainerDataSource(), detail);


    }

    private static void validate(Container.Indexed ds, AbstractComponent component) {
        ds.getItemIds().stream().map(ds::getItem).forEach(i -> {
            Property rightVal = i.getItemProperty("RIGHT");
            Property leftVal = i.getItemProperty("LEFT");

            final Object val;
            switch((String) leftVal.getValue()) {
                case "UI" : val =component.getUI(); break;
                case "caption": val = component.getCaption(); break;
                case "enabled": val = component.isEnabled(); break;
                default:
                    val = null;
                    fail("Unknown property");
            }

            assertThat(Optional.ofNullable(val).orElse(""), is(rightVal.getValue()));
        });
    }

}
