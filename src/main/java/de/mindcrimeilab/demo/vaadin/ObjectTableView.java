package de.mindcrimeilab.demo.vaadin;


import com.vaadin.data.Item;
import com.vaadin.data.util.BeanItem;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.AbstractComponent;
import com.vaadin.ui.Grid;
import com.vaadin.ui.VerticalLayout;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;


/**
 * Simple page containing both tables, the master (list) table and the
 * details table.
 */
@SpringView(name = ObjectTableView.VIEW_NAME)
class ObjectTableView extends VerticalLayout implements View {

    public static final String VIEW_NAME = "ObjectTableView";
    private static final String LEFT = "LEFT";
    private static final String RIGHT = "RIGHT";
    private static final List<String> VISIBLE_PROPERTIES = Arrays.asList("UI", "caption", "enabled");

    public static final String OBJECT_LIST_ID = "#objectList";
    public static final String OBJECT_DETAIL_ID = "#objectDetail";

    private Grid objectList;
    private Grid objectDetail;


    @SuppressWarnings("unused")
    @PostConstruct
    void init() {
        setMargin(true);
        setSpacing(true);
        setSizeFull();

        objectList = new Grid();
        objectList.setCaption("caption desc");
        objectList.setId(OBJECT_LIST_ID);
        objectList.setSizeFull();
        objectList.setSelectionMode(Grid.SelectionMode.SINGLE);

        objectList.addItemClickListener(this::onItemClicked);

        objectDetail = new Grid();
        objectDetail.setId(OBJECT_DETAIL_ID);
        objectDetail.setSizeFull();

        addComponent(objectList);
        addComponent(objectDetail);

    }


    @Override
    public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent) {
        BeanItemContainer<AbstractComponent> c = new BeanItemContainer<>(AbstractComponent.class);
        c.addAll(Arrays.asList(objectDetail, objectList));
        objectList.setContainerDataSource(c);

        IndexedContainer ci = new IndexedContainer();
        ci.addContainerProperty(LEFT, String.class, null);
        ci.addContainerProperty(RIGHT, Object.class, null);

        objectDetail.setContainerDataSource(ci);
    }


    void onItemClicked(ItemClickEvent evt) {
        Item item = evt.getItem();
        if (item instanceof BeanItem) {
            @SuppressWarnings("unchecked") final BeanItem<AbstractComponent> bi = (BeanItem<AbstractComponent>) item;

            @SuppressWarnings("unchecked") IndexedContainer container = (IndexedContainer) objectDetail.getContainerDataSource();
            container.removeAllItems();

            bi.getItemPropertyIds().stream().filter(VISIBLE_PROPERTIES::contains).forEach(pid -> {
                Item i = container.getItem(container.addItem());
                i.getItemProperty(LEFT).setValue(pid);
                i.getItemProperty(RIGHT).setValue(Optional.ofNullable(bi.getItemProperty(pid).getValue()).orElse(""));
            });
        }
    }
}

