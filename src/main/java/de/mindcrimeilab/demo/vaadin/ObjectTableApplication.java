package de.mindcrimeilab.demo.vaadin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Simple application showing how to render the column details
 * of one table element as rows in another one.
 */
@SpringBootApplication
public class ObjectTableApplication {

	public static void main(String[] args) {
		SpringApplication.run(ObjectTableApplication.class, args);
	}
}
